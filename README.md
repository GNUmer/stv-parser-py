# stv-parser-py
This is a Python implementation of a vote parser for [stv.py](https://github.com/vihreatnuoret/stv). This script collection is meant to be used to generate voting tokens based on data from a CSV file and purge the output of votes based on an election held with Cryptpad. The parser checks the votes against the generated vote tokens, purges duplicate votes/invalid votes and parses the data to be used by stv.py. 

For the time being, stv-parser-py is meant for testing use or minimal production use. The project might be rewritten in Golang/Rustlang depending on how the project is used.

# Generate.py

```
python3 generate.py
Enter filepath name: emails.csv
```

Generate.py is the backbone of the script collection. The script takes in a CSV file exported from the voter registration form and outputs a votes.csv file containing the email addresses and the voting tokens. The votes.csv file will be used later for handing out the voting tokens to the voters and validating the votes with clean.py. As the script is still in testing phase, the script is hardcoded to read columns with the name `Sähköpostiosoitteet`. If you want to name the column for email addresses in another way, you need to change the hardcoded `Sähköpostiosoitteet` to your preferred email column name.

# Clean.py

```
python3 clean.py
Anna äänestäjätiedosto: votes.csv
Anna äänestystiedosto: name_of_form_results.csv
```

Clean.py is used for validating the votes exported from a Cryptpad form and validating votes against votes.csv. If the correct data is given to the script, the script will output data readable to stv.py in the voting.csv file. 

The script has some hardcoded portions. As CryptPad outputs the time and CryptPad username before the data input from the voter, clean.py drops the first two columns and reads the only the last two columns. The script is hardcoded to read the voting tokens from the third column. If you want to name the third column something else than `Koodi`, remove all references to `Koodi` in the script.

# Count.py

Count.py is mainly used for development and reads the amount of lines from the emails.csv file.

