import pandas as pd
import numpy as np
import threading

filepath = input('Enter filepath name: ')
df = pd.read_csv(filepath)

df.drop_duplicates(subset=['Sähköpostiosoitteet'],inplace = True)

rows = len(df.index)

LENGTH = 20
NO_CODES = rows

alphabet = list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
np_alphabet = np.array(alphabet, dtype="|U1")
np_codes = np.random.choice(np_alphabet, [NO_CODES, LENGTH])
codes = ["".join(np_codes[i]) for i in range(len(np_codes))]

def generatesum():
    df['Koodi'] = codes

threads = []

for i in range(3):
    t = threading.Thread(target=generatesum)
    t.daemon = True
    threads.append(t)

for i in range(3):
    threads[i].start()

for i in range(3):
    threads[i].join()

df.to_csv("votes.csv")

print(df)

