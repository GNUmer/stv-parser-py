import pandas as pd
import csv

inputfile = input('Anna äänestäjätiedosto: ')
votefile = input('Anna äänestystiedosto: ')
df1 = pd.read_csv(inputfile, header = 0, sep=",")
df2 = pd.read_csv(votefile, header = 0, sep=",")
df = df2[df2['Koodi'].isin(df1['Koodi'])].drop_duplicates(subset=['Koodi'], keep='first')
#df.columns.values[3] = "results'"
df = df.replace(',',', ', regex=True)
df3 = df.iloc[:,3]
#df3 = df.loc['votetokens']
df4 = df3.dropna()
remove_end = df4.replace('Name: Votes, dtype: object', '')

with open('voting.csv', 'w') as file:
    file.write(remove_end.to_string(index=False))

#print(remove_end.to_string(index=False))
